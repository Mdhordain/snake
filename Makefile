SDL_FLAGS=$$(sdl2-config --cflags --libs)
CFLAGS=-std=c11 -Wall -W -pedantic
CC=cc
EXEC=snake

all: $(EXEC)

snake: window.o snake.o main.o
	$(CC) -o $@ $^ $(CFLAGS) $(SDL_FLAGS)

window.o: window.c
	$(CC) -c -o $@ $^ $(CFLAGS) $(SDL_FLAGS)

snake.o: snake.c
	$(CC) -c -o $@ $^ $(CFLAGS) $(SDL_FLAGS)

main.o: main.c
	$(CC) -c -o $@ $^ $(CFLAGS) $(SDL_FLAGS)

clean:
	rm -rf *.o

mrproper:
	rm -rf $(EXEC)
	rm -rf *.o
