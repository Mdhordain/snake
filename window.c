#include "window.h"

SDL_Window *window;
SDL_Renderer *renderer;

SDL_Surface *block_sprite;
SDL_Surface *grass_sprite;
SDL_Surface *fruit_sprite;
SDL_Texture *block_texture;
SDL_Texture *grass_texture;
SDL_Texture *fruit_texture;

int init_window() {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
        handle_error("initialization");
        return 1;
    }

    window = SDL_CreateWindow("SNAKE", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,
                              WINDOW_HEIGHT, 0);

    if (!window) {
        handle_error("window creation");
        return 1;
    }

    renderer = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (!renderer) {
        handle_error("renderer creation");
        return 1;
    }

    block_sprite = SDL_LoadBMP("img/block.bmp");
    grass_sprite = SDL_LoadBMP("img/grass.bmp");
    fruit_sprite = SDL_LoadBMP("img/fruit.bmp");

    if (block_sprite && grass_sprite && fruit_sprite) {
        block_texture = SDL_CreateTextureFromSurface(renderer, block_sprite);
        grass_texture = SDL_CreateTextureFromSurface(renderer, grass_sprite);
        fruit_texture = SDL_CreateTextureFromSurface(renderer, fruit_sprite);

        if (!block_sprite || !grass_sprite || !fruit_sprite) {
            handle_error("texture creation");
            return 1;
        }
    } else {
        handle_error("sprite cration");
        return 1;
    }

    return 0;
}

void close_window() {
    SDL_DestroyTexture(fruit_texture);
    SDL_DestroyTexture(block_texture);
    SDL_DestroyTexture(grass_texture);
    SDL_FreeSurface(fruit_sprite);
    SDL_FreeSurface(block_sprite);
    SDL_FreeSurface(grass_sprite);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void window_render(char (*map)[WINDOW_WIDTH / 10]) {
    uint32_t x = 0;
    uint32_t y = 0;

    for (int i = 0; i < WINDOW_HEIGHT / 10; i++) {
        for (int j = 0; j < WINDOW_WIDTH / 10; j++) {
            if (map[i][j] == WALL || map[i][j] == SNAKE_NODE) {
                SDL_Rect dest = {x, y, 10, 10};
                SDL_RenderCopy(renderer, block_texture, NULL, &dest);
            } else if (map[i][j] == FRUIT) {
                SDL_Rect dest = {x, y, 10, 10};
                SDL_Rect src = {2, 5, fruit_sprite->w / 9,
                                fruit_sprite->h / 11};
                SDL_RenderCopy(renderer, fruit_texture, &src, &dest);
            } else {
                SDL_Rect dest = {x, y, 10, 10};
                SDL_RenderCopy(renderer, grass_texture, NULL, &dest);
            }
            x += 10;
        }
        y += 10;
        x = 0;
    }
    SDL_RenderPresent(renderer);
}

void handle_error(const char *msg) {
    fprintf(stderr, "SDL error : %s (%s)", msg, SDL_GetError());
}
