#include "snake.h"

static struct snake *main_snake;
static struct fruit *fruit;

static const uint32_t snake_w = WINDOW_WIDTH / 10;
static const uint32_t snake_h = WINDOW_HEIGHT / 10;

static char (*map)[WINDOW_WIDTH / 10];

static bool running;

void init_game() {
    main_snake = snake_init(10, snake_w / 2, snake_h / 2);
    map = malloc(sizeof(char[snake_h][snake_w]));
    draw_map();
    new_fruit();
}

void end_game() {
    snake_kill(main_snake);
    free(map);
    free(fruit);
}

void draw_map() {
    for (uint32_t i = 0; i < snake_h; i++) {
        map[i][0] = WALL;
        map[i][snake_w - 1] = '#';
    }
    for (uint32_t i = 0; i < snake_w; i++) {
        map[0][i] = WALL;
        map[snake_h - 1][i] = '#';
    }
}

void clear_map() {
    for (uint32_t i = 0; i < snake_h; i++) {
        for (uint32_t j = 0; j < snake_w; j++) {
            map[i][j] = 0;
        }
    }
}

void print_map() {
    // debug purposes
    for (uint32_t i = 0; i < snake_h; i++) {
        for (uint32_t j = 0; j < snake_w; j++) {
            if (map[i][j] == SNAKE_NODE || map[i][j] == WALL) {
                printf("%c", map[i][j]);
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }
}

void handle_event() {
    SDL_FlushEvent(SDL_MOUSEMOTION);
    SDL_Event event;
    SDL_PollEvent(&event);

    switch (event.type) {
    case SDL_KEYDOWN:
        SDL_FlushEvent(SDL_KEYDOWN);
        switch (event.key.keysym.sym) {
        case SDLK_UP:
            if (main_snake->head->direction != DOWN) {
                main_snake->head->direction = UP;
                main_snake->head->signal = UP;
            }
            break;
        case SDLK_DOWN:
            if (main_snake->head->direction != UP) {
                main_snake->head->direction = DOWN;
                main_snake->head->signal = DOWN;
            }
            break;
        case SDLK_LEFT:
            if (main_snake->head->direction != RIGHT) {
                main_snake->head->direction = LEFT;
                main_snake->head->signal = LEFT;
            }
            break;
        case SDLK_RIGHT:
            if (main_snake->head->direction != LEFT) {
                main_snake->head->direction = RIGHT;
                main_snake->head->signal = RIGHT;
            }
            break;
        }
        break;
    case SDL_QUIT:
        SDL_FlushEvent(SDL_KEYDOWN);
        running = false;
        break;
    default:
        break;
    }
}

void render() {
    clear_map();
    draw_map();

    map[fruit->y][fruit->x] = FRUIT;

    node *a = main_snake->head;

    while (a != NULL) {
        map[a->y][a->x] = SNAKE_NODE;
        a = a->next;
    }
    window_render(map);
}

void run_game() {
    running = true;

    while (running) {
        move();
        handle_event();
        check_collisions();
        render();
        SDL_Delay(50);
    }
}

void game_over() {
    running = false;
}

struct snake *snake_init(int size, uint32_t x, uint32_t y) {
    struct snake *new_snake;
    new_snake = malloc(sizeof(struct snake));
    new_snake->head = node_init(NULL, x, y);

    node *a = new_snake->head;
    for (int i = 0; i < size; i++) {
        x = x - 1;
        a->next = node_init(a, x, y);
        a = a->next;
    }

    new_snake->tail = a;

    return new_snake;
}

void snake_kill(struct snake *snake) {
    node *a = snake->head;
    while (a != NULL) {
        node *b = a->next;
        free(a);
        a = b;
    }
    free(snake);
}

void move() {
    node *a = main_snake->head;

    while (a != NULL) {
        switch (a->direction) {
        case UP:
            a->y--;
            break;
        case DOWN:
            a->y++;
            break;
        case LEFT:
            a->x--;
            break;
        case RIGHT:
            a->x++;
            break;
        case NONE:
        default:
            break;
        }
        a->signal = a->direction;
        a = a->next;
    }

    a = main_snake->head;

    while (a != NULL) {
        if (a->next != NULL && a->signal != NONE) {
            a->next->direction = a->signal;
            a->signal = NONE;
        }
        a = a->next;
    }
}

void check_collisions() {
    bool collision = false;
    node *a = main_snake->head;

    if (a->x == 0 || a->x == snake_w - 1 || a->y == 0 || a->y == snake_h - 1) {
        collision = true;
    }

    a = a->next;

    while (a != NULL) {
        if (main_snake->head->x == a->x && main_snake->head->y == a->y) {
            collision = true;
        }
        a = a->next;
    }

    if (collision) {
        game_over();
    }

    if (main_snake->head->x == fruit->x && main_snake->head->y == fruit->y) {
        grow();
        new_fruit();
    }
}

void grow() {
    switch (main_snake->tail->direction) {
    case UP:
        main_snake->tail->next = node_init(
            main_snake->tail, main_snake->tail->x, main_snake->tail->y + 1);
        break;
    case DOWN:
        main_snake->tail->next = node_init(
            main_snake->tail, main_snake->tail->x, main_snake->tail->y - 1);
        break;
    case LEFT:
        main_snake->tail->next = node_init(
            main_snake->tail, main_snake->tail->x + 1, main_snake->tail->y);
        break;
    case RIGHT:
        main_snake->tail->next = node_init(
            main_snake->tail, main_snake->tail->x - 1, main_snake->tail->y);
        break;
    case NONE:
    default:
        break;
    }
    main_snake->tail = main_snake->tail->next;
}

node *node_init(node *prev, uint32_t x, uint32_t y) {
    node *new_node;
    new_node = malloc(sizeof(node));

    if (prev == NULL) {
        new_node->direction = RIGHT;
    } else {
        new_node->direction = prev->direction;
    }

    new_node->x = x;
    new_node->y = y;
    new_node->next = NULL;
    new_node->signal = NONE;

    return new_node;
}

void new_fruit() {
    fruit = malloc(sizeof(struct fruit));
    fruit->x = rand() % snake_w + 1;
    fruit->y = rand() % snake_h + 1;
    if (fruit->x == snake_w || fruit->y == snake_h) {
        new_fruit();
    }
}
