#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#define WALL '#'
#define SNAKE_NODE 'o'
#define FRUIT '*'

int init_window();
void close_window();
void window_render();
void handle_error(const char *);
