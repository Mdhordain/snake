#pragma once

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "window.h"

enum direction { NONE, UP, DOWN, LEFT, RIGHT };

struct snake_node {
    uint32_t x;
    uint32_t y;
    enum direction direction;
    enum direction signal;
    struct snake_node *next;
};
typedef struct snake_node node;

struct snake {
    node *head;
    node *tail;
};

struct fruit {
    uint32_t x;
    uint32_t y;
};

#define FRUIT '*'
#define SNAKE_NODE 'o'
#define WALL '#'

void init_game();
void end_game();
void handle_event();
void draw_map();
void clear_map();
void render();
void run_game();
void game_over();
struct snake *snake_init(int size, uint32_t, uint32_t);
void snake_kill(struct snake *);
void move();
void check_collisions();
node *node_init(node *, uint32_t, uint32_t);
void new_fruit();
void grow();
