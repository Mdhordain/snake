#include <stdio.h>
#include <stdlib.h>

#include "snake.h"
#include "window.h"

int main() {
    if (!init_window()) {
        init_game();
        run_game();
        close_window();
        end_game();
    }
    return 0;
}
